function monthDiff(fromDate, toDate) {
    let months;
    months = (toDate.getFullYear() - fromDate.getFullYear()) * 12;
    months -= fromDate.getMonth();
    months += toDate.getMonth();
    return months <= 0 ? 0 : months;
}

function validateDates(element) {
	let fromDateInput = document.getElementById("fromDate");
	let fromDate = new Date(fromDateInput.value);
	let toDateInput = document.getElementById("toDate");
    let toDate = new Date(toDateInput.value);
    if(toDate < fromDate ) {
        if(element.id == "toDate") {
            fromDateInput.value = "";
        } else {
            toDateInput.value = "";
        }
    }
}

function validateDatesInterval(element) {
	let fromDateInput = document.getElementById("fromDate");
	let fromDate = new Date(fromDateInput.value);
	let toDateInput = document.getElementById("toDate");
    let toDate = new Date(toDateInput.value);
    if(toDate < fromDate ) {
        if(element.id == "toDate") {
            fromDateInput.value = "";
        } else {
            toDateInput.value = "";
        }
    }
    if(monthDiff(fromDate, toDate) >= 12) {
        if(element.id == "toDate") {
            fromDateInput.value = "";
        } else {
            toDateInput.value = "";
        }
    }
}


