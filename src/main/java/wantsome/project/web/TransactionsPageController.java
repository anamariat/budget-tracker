package wantsome.project.web;

import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.EscapeTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionFullDto;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.CategoriesDao;
import wantsome.project.db.service.TransactionsDao;

import java.sql.Date;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class TransactionsPageController {

    private static EscapeTool esc = new EscapeTool();
    private static MathTool mt = new MathTool();
    private static NumberTool nt = new NumberTool();
    private static DateTool dateTool = new DateTool();

    public static String showTransactionsPage(Request req, Response res) {

        List<TransactionFullDto> allTransactions;
        Date fromDate;
        Date toDate;
        SortBy sortBy = SortBy.DATE;
        String filterBy = req.queryParams("filterBy");
        Type[] allTypes = Type.values();

        try {
            fromDate = Date.valueOf(req.queryParams("fromDate"));
            toDate = Date.valueOf(req.queryParams("toDate"));
        } catch (Exception e) {
            fromDate = null;
            toDate = null;
        }

        if (filterBy != null && !filterBy.isEmpty()) {
            allTransactions = getFilteredList(allTypes, filterBy, fromDate, toDate);
        } else if (fromDate != null && toDate != null) {
            allTransactions = TransactionsDao.getPerDateInterval(fromDate, toDate);
        } else {
            allTransactions = TransactionsDao.getAll();
        }

        Comparator<TransactionFullDto> sortByAmountThenByDateComparator =
                Comparator.comparingDouble(TransactionFullDto::getAmount)
                        .thenComparing(TransactionFullDto::getDate);

        try {
            sortBy = SortBy.valueOf(req.queryParams("sortBy"));
            if (sortBy.equals(SortBy.AMOUNT)) {
                allTransactions.sort(sortByAmountThenByDateComparator);
            }
        } catch (Exception ignored) {
        }

        Map<String, Object> model = new HashMap<>();
        model.put("fromDate", fromDate);
        model.put("toDate", toDate);
        model.put("sortBy", sortBy);
        model.put("filterBy", filterBy);
        model.put("allTransactions", allTransactions);
        model.put("allCategories", CategoriesDao.getAll());
        model.put("allTypes", allTypes);
        model.put("esc", esc);
        model.put("mt", mt);
        model.put("nt", nt);
        model.put("dateTool", dateTool);
        return render(model, "transactions.vm");
    }

    public static Object handleDeleteRequest(Request req, Response res) {
        String id = req.params("id");
        String fromDate = req.queryParams("fromDate");
        String toDate = req.queryParams("toDate");
        String sortBy = req.queryParams("sortBy");
        String filterBy = req.queryParams("filterBy");
        String filters = "fromDate=" + fromDate + "&toDate=" + toDate +
                "&sortBy=" + sortBy + "&filterBy=" + esc.url(filterBy);

        try {
            TransactionsDao.delete(Long.parseLong(id));
        } catch (Exception e) {
            System.out.println("Error deleting transaction with id '" + id + "': " + e.getMessage());
        }
        res.redirect("/transactions?" + filters);
        return res;
    }

    private static List<TransactionFullDto> getFilteredList
            (Type[] allTypes, String filterBy, Date fromDate, Date toDate) {

        boolean isType = false;

        for (Type type : allTypes) {
            if (filterBy.equalsIgnoreCase(type.name())) {
                isType = true;
                break;
            }
        }

        if (isType) {
            return TransactionsDao.getByType(Type.valueOf(filterBy), fromDate, toDate);
        } else {
            return TransactionsDao.getByCategory(filterBy, fromDate, toDate);
        }
    }

    private enum SortBy {
        DATE,
        AMOUNT;
    }
}
