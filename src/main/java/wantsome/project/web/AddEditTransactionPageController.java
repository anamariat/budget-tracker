package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionDto;
import wantsome.project.db.service.CategoriesDao;
import wantsome.project.db.service.TransactionsDao;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditTransactionPageController {

    public static String showAddForm(Request req, Response res) {
        return renderAddUpdateForm("", "", "", "", "", "");
    }

    public static String showUpdateForm(Request req, Response res) {
        String id = req.params("id");
        Optional<TransactionDto> optTransaction = TransactionsDao.get(Long.parseLong(id));

        if (!optTransaction.isPresent()) {
            throw new RuntimeException("Error: Transaction with id " + id + " not found!");
        }

        TransactionDto transaction = optTransaction.get();
        return renderAddUpdateForm(
                String.valueOf(transaction.getId()),
                String.valueOf(transaction.getCategoryId()),
                String.valueOf(transaction.getDate()),
                transaction.getDetails(),
                String.valueOf(transaction.getAmount()),
                ""
        );
    }

    public static Object handleAddUpdateRequest(Request req, Response res) {
        String id = req.queryParams("id");
        String categoryId = req.queryParams("categoryId");
        String details = req.queryParams("transactionDetails");
        String amount = req.queryParams("transactionAmount");
        String date = req.queryParams("transactionDate");

        try {
            TransactionDto transaction = validateAndBuildTransaction(id, categoryId, details, amount, date);
            boolean idAddCase = id == null || id.isEmpty();

            if (idAddCase) {
                TransactionsDao.insert(transaction);
            } else {
                TransactionsDao.update(transaction);
            }

            res.redirect("/transactions");
            return res;
        } catch (Exception e) {
            return renderAddUpdateForm(id, categoryId, date, details, amount, e.getMessage());
        }
    }

    private static TransactionDto validateAndBuildTransaction(String id, String categoryId, String details,
                                                              String amount, String date) {

        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;
        long catIdValue = Long.parseLong(categoryId);

        double amountValue = -1;
        try {
            amountValue = Double.parseDouble(amount);
        } catch (Exception e) {
            throw new RuntimeException("Error: Invalid amount! It should be equal to or greater than 0.01!");
        }

        Date dateValue = null;
        try {
            dateValue = Date.valueOf(date);
        } catch (Exception e) {
            throw new RuntimeException("Error: Invalid date! " + date);
        }

        return new TransactionDto(idValue, catIdValue, dateValue, details, amountValue);
    }

    private static String renderAddUpdateForm(String id, String prevCategId, String prevDate,
                                              String prevDetails, String prevAmount, String errorMessage) {

        Map<String, Object> model = new HashMap<>();
        model.put("id", id);
        model.put("prevCategId", prevCategId);
        model.put("prevDate", prevDate);
        model.put("prevDetails", prevDetails);
        model.put("prevAmount", prevAmount);
        model.put("isAdd", id == null || id.isEmpty());
        model.put("allCategories", CategoriesDao.getAll());
        model.put("errorMessage", errorMessage);
        return render(model, "add_edit_transaction.vm");
    }
}
