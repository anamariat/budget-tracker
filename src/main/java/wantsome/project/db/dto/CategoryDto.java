package wantsome.project.db.dto;

import java.util.Objects;

public class CategoryDto {
    private final long id;
    private final String description;
    private final Type type;

    public CategoryDto(long id, String description, Type type) {
        this.id = id;
        this.description = description;
        this.type = type;
    }

    public CategoryDto(String description, Type type) {
        this(-1, description, type);
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDto that = (CategoryDto) o;
        return id == that.id &&
                Objects.equals(description, that.description) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, type);
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", type=" + type +
                '}';
    }
}
