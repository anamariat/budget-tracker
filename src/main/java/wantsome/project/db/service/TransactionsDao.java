package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.TransactionDto;
import wantsome.project.db.dto.TransactionFullDto;
import wantsome.project.db.dto.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static wantsome.project.db.service.tables.CategoriesTable.*;
import static wantsome.project.db.service.tables.TransactionsTable.*;

public class TransactionsDao {

    private static TransactionDto extractTransactionFromResult(ResultSet r) throws SQLException {

        long id = r.getLong(FLD_TRANS_ID);
        long categoryId = r.getLong(FLD_TRANS_CAT_ID);
        Date date = r.getDate(FLD_TRANS_DATE);
        String details = r.getString(FLD_TRANS_DETAILS);
        double amount = r.getDouble(FLD_TRANS_AMOUNT);

        return new TransactionDto(id, categoryId, date, details, amount);
    }

    private static TransactionFullDto extractFullTransactionFromResult(ResultSet r) throws SQLException {

        long id = r.getLong(FLD_TRANS_ID);
        long categoryId = r.getLong(FLD_TRANS_CAT_ID);
        String categoryDescription = r.getString("CATEGORY_DESCRIPTION");
        Type type = Type.valueOf(r.getString("TYPE"));
        Date date = r.getDate(FLD_TRANS_DATE);
        String details = r.getString(FLD_TRANS_DETAILS);
        double amount = r.getDouble(FLD_TRANS_AMOUNT);

        return new TransactionFullDto(id, categoryId, categoryDescription, type, date, details, amount);
    }

    public static void insert(TransactionDto transaction) {

        String sql = "INSERT INTO " + TRANSACTIONS_TABLE_NAME + " (" +
                FLD_TRANS_CAT_ID + ", " + FLD_TRANS_DATE + ", " + FLD_TRANS_DETAILS + ", " + FLD_TRANS_AMOUNT + ")" +
                " VALUES (?,?,?,?)";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setLong(1, transaction.getCategoryId());
            ps.setDate(2, transaction.getDate());
            ps.setString(3, transaction.getDetails());
            ps.setDouble(4, transaction.getAmount());

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while saving new transaction: " + e.getLocalizedMessage());
        }
    }

    public static List<TransactionFullDto> getAll() {

        String sql = "SELECT T.*, " +
                FLD_CAT_DESCRIPTION + " AS CATEGORY_DESCRIPTION, " +
                FLD_CAT_TYPE + " AS TYPE " +
                "FROM " + TRANSACTIONS_TABLE_NAME + " T " +
                "JOIN " + CATEGORIES_TABLE_NAME + " C ON T." + FLD_TRANS_CAT_ID + " = C." + FLD_CAT_ID +
                " ORDER BY " + FLD_TRANS_DATE;

        List<TransactionFullDto> transactions = new ArrayList<>();

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                transactions.add(extractFullTransactionFromResult(rs));
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while loading all transactions: " + e.getLocalizedMessage());
        }

        return transactions;
    }

    public static Optional<TransactionDto> get(long id) {

        String sql = "SELECT T.* " +
                "FROM " + TRANSACTIONS_TABLE_NAME + " T " +
                " WHERE T." + FLD_TRANS_ID + " = ?";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    TransactionDto transaction = extractTransactionFromResult(rs);
                    return Optional.of(transaction);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while loading transaction " + id + ": " + e.getLocalizedMessage());
        }

        return Optional.empty();
    }

    public static void update(TransactionDto transaction) {

        String sql = "UPDATE " + TRANSACTIONS_TABLE_NAME +
                " SET " + FLD_TRANS_CAT_ID + " = ?, " +
                FLD_TRANS_DATE + " = ?, " +
                FLD_TRANS_DETAILS + " = ?, " +
                FLD_TRANS_AMOUNT + " = ? " +
                "WHERE " + FLD_TRANS_ID + " = ?";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setLong(1, transaction.getCategoryId());
            ps.setDate(2, transaction.getDate());
            ps.setString(3, transaction.getDetails());
            ps.setDouble(4, transaction.getAmount());
            ps.setLong(5, transaction.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error while updating transaction " + transaction + ": " + e.getLocalizedMessage());
        }
    }

    public static void delete(long id) {

        String sql = "DELETE FROM " + TRANSACTIONS_TABLE_NAME +
                " WHERE " + FLD_TRANS_ID + " = ?";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while deleting transaction " + id + ": " + e.getLocalizedMessage());
        }
    }

    public static List<TransactionFullDto> getByType(Type type, Date fromDate, Date toDate) {
        String sql = null;

        if (fromDate != null && toDate != null) {
            sql = "SELECT T.*, " +
                    FLD_CAT_DESCRIPTION + " AS CATEGORY_DESCRIPTION, " +
                    FLD_CAT_TYPE + " AS TYPE " +
                    "FROM " + TRANSACTIONS_TABLE_NAME + " T " +
                    "INNER JOIN " + CATEGORIES_TABLE_NAME + " C ON T." + FLD_TRANS_CAT_ID + " = C." + FLD_CAT_ID +
                    " WHERE " + FLD_CAT_TYPE + " = ? AND " +
                    FLD_TRANS_DATE + " BETWEEN ? AND ?" +
                    " ORDER BY " + FLD_TRANS_DATE;
        } else {
            sql = "SELECT T.*, " +
                    FLD_CAT_DESCRIPTION + " AS CATEGORY_DESCRIPTION, " +
                    FLD_CAT_TYPE + " AS TYPE " +
                    "FROM " + TRANSACTIONS_TABLE_NAME + " T " +
                    "INNER JOIN " + CATEGORIES_TABLE_NAME + " C ON T." + FLD_TRANS_CAT_ID + " = C." + FLD_CAT_ID +
                    " WHERE " + FLD_CAT_TYPE + " = ?" +
                    " ORDER BY " + FLD_TRANS_DATE;
        }

        List<TransactionFullDto> transactionsByType = new ArrayList<>();

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setString(1, type.name());
            if (fromDate != null && toDate != null) {
                ps.setDate(2, fromDate);
                ps.setDate(3, toDate);
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    transactionsByType.add(extractFullTransactionFromResult(rs));
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while loading " + type + "transactions: " + e.getLocalizedMessage());
        }

        return transactionsByType;
    }

    public static List<TransactionFullDto> getByCategory(String description, Date fromDate, Date toDate) {
        String sql = null;

        if (fromDate != null && toDate != null) {
            sql = "SELECT T.*, " +
                    FLD_CAT_DESCRIPTION + " AS CATEGORY_DESCRIPTION, " +
                    FLD_CAT_TYPE + " AS TYPE " +
                    "FROM " + TRANSACTIONS_TABLE_NAME + " T " +
                    "INNER JOIN " + CATEGORIES_TABLE_NAME + " C ON T." + FLD_TRANS_CAT_ID + " = C." + FLD_CAT_ID +
                    " WHERE " + FLD_CAT_DESCRIPTION + " = ? AND " +
                    FLD_TRANS_DATE + " BETWEEN ? AND ?" +
                    " ORDER BY " + FLD_TRANS_DATE;
        } else {
            sql = "SELECT T.*, " +
                    FLD_CAT_DESCRIPTION + " AS CATEGORY_DESCRIPTION, " +
                    FLD_CAT_TYPE + " AS TYPE " +
                    "FROM " + TRANSACTIONS_TABLE_NAME + " T " +
                    "INNER JOIN " + CATEGORIES_TABLE_NAME + " C ON T." + FLD_TRANS_CAT_ID + " = C." + FLD_CAT_ID +
                    " WHERE " + FLD_CAT_DESCRIPTION + " = ?" +
                    " ORDER BY " + FLD_TRANS_DATE;
        }

        List<TransactionFullDto> transactionsByDescription = new ArrayList<>();

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setString(1, description);
            if (fromDate != null && toDate != null) {
                ps.setDate(2, fromDate);
                ps.setDate(3, toDate);
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    transactionsByDescription.add(extractFullTransactionFromResult(rs));
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while loading transactions in " + description + " category: " + e.getLocalizedMessage());
        }

        return transactionsByDescription;
    }

    public static List<TransactionFullDto> getPerDateInterval(Date fromDate, Date toDate) {

        String sql = "SELECT T.*, " +
                FLD_CAT_DESCRIPTION + " AS CATEGORY_DESCRIPTION, " +
                FLD_CAT_TYPE + " AS TYPE " +
                "FROM " + TRANSACTIONS_TABLE_NAME + " T " +
                "INNER JOIN " + CATEGORIES_TABLE_NAME + " C ON T." + FLD_TRANS_CAT_ID + " = C." + FLD_CAT_ID +
                " WHERE " + FLD_TRANS_DATE + " BETWEEN ? AND ?" +
                " ORDER BY " + FLD_TRANS_DATE;

        List<TransactionFullDto> transactionsBetweenDates = new ArrayList<>();

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setDate(1, fromDate);
            ps.setDate(2, toDate);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    transactionsBetweenDates.add(extractFullTransactionFromResult(rs));
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while loading transactions between " +
                    fromDate + " and " + toDate + ": " + e.getLocalizedMessage());
        }

        return transactionsBetweenDates;
    }
}