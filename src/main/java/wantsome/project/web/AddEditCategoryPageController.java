package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.CategoryDto;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.CategoriesDao;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditCategoryPageController {

    public static String showAddForm(Request req, Response res) {
        return renderAddUpdateForm("", "", "", "");
    }

    public static String showUpdateForm(Request req, Response res) {
        String id = req.params("id");
        Optional<CategoryDto> optCategory = CategoriesDao.get(Long.parseLong(id));
        if (optCategory.isPresent()) {
            CategoryDto category = optCategory.get();
            return renderAddUpdateForm(
                    String.valueOf(category.getId()),
                    category.getDescription(),
                    String.valueOf(category.getType().getLabel()),
                    "");
        }
        throw new RuntimeException("Error: category " + id + " not found!");
    }

    public static Object handleAddUpdateRequest(Request req, Response res) {
        String id = req.queryParams("id");
        String description = req.queryParams("description");
        String type = req.queryParams("type");

        try {
            CategoryDto category = validateAndBuildCategory(id, description, type);
            boolean idAddCase = id == null || id.isEmpty();

            if (idAddCase) {
                CategoriesDao.insert(category);
            } else {
                CategoriesDao.updateDescription(category.getDescription(), category.getId());
            }

            res.redirect("/categories");
            return res;

        } catch (Exception e) {
            return renderAddUpdateForm(id, description, type, e.getMessage());
        }
    }

    private static CategoryDto validateAndBuildCategory(String id, String description, String type) {
        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;

        if (description == null || description.isEmpty()) {
            throw new RuntimeException("Error: Description is required!");
        }

        Type typeValue = null;
        try {
            typeValue = Type.valueOf(type.toUpperCase());
        } catch (Exception e) {
            throw new RuntimeException("Error: Type is required!");
        }

        return new CategoryDto(idValue, description, typeValue);
    }

    private static String renderAddUpdateForm(String id, String prevDescription, String prevType, String errorMessage) {

        Map<String, Object> model = new HashMap<>();
        model.put("id", id);
        model.put("prevDescription", prevDescription);
        model.put("prevType", prevType);
        model.put("isAdd", id == null || id.isEmpty());
        model.put("errorMessage", errorMessage);
        return render(model, "add_edit_category.vm");
    }

}
