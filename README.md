## Wantsome - Budget Tracker

## Description
This is a small budget management app for personal use. It tracks inflow and outflow, 
allowing the user to gain more control over his spendings by getting reports 
on his transactions.

## Technologies
Budget Tracker is a **web application**. The **technologies** used for developing 
it are the following:
- Java 11 (for the main code)
- [SQLite](https://www.sqlite.org/index.html) 3.28.0, which uses SQL and JDBC to connect Java to a database (to store, modify and access data)
- [Spark Framework](http://sparkjava.com/) 2.9.1 (which includes an embedded web server, Jetty)
- [Velocity templating engine](https://velocity.apache.org/) 3.0 (to separate Java code from the web page design)
- HTML, CSS and a little JavaScript (for UI design)
- [Chart.js](https://www.chartjs.org/) 2.0 library to display reports
- [Font Awesome](https://fontawesome.com/) 5.8.2 library
- [JUnit](https://junit.org/junit4/) 4.12 library (to create unit tests for database)

**Code structure**

Java classes are organized in packages, depending on their roles:
- **root** package: contains the main class, `Main.java`, which starts the program
- **business**: contains `TransactionsStatistics.java` which computes all the necessary
statistics used for creating reports
- **db**: contains the database part, including DTOs and DAOs, as well as the code 
to initialize and connect to the db
- **web**: contains the controllers necessary to access and render the web pages

All the web resources are found in `main/resources` folder:
- `/public`: contains static resources to be served by the web server directly 
(images, css files, js files)
- directly under `/resources`: Velocity templates with code to create the 
design of the web pages

## Setup
There is no setup needed. If the database is missing (for example, on first startup), 
it will create a new database (of type SQLite, stored in a local file named 'budget_tracker.db'), 
which the application will use to save future data.

Once the web app starts, navigate with a web browser at url: <http://localhost:4567/main>

## Features
 ![Dashboard](src/docs/dashboard.png)

The application supports a variety of actions, as:

 - Add/update/delete and categorize transactions
 
  ![Add-transaction](src/docs/add-transaction.gif)
 
 - Add/update/delete/view categories
 
 ![Categories](src/docs/categories.png)
 
 - View all transactions or only the ones added in a certain period of time
 - Sort transactions by date, amount and filter them by category or by type 
 (income/expense)
 
 ![Filter-transactions](src/docs/transaction-filters.gif)
 
 - Show monthly reports on expenses, income and balance (for a custom date range, 
1 year maximum)

 ![Reports](src/docs/reports.gif)

To-do list:
- Support for importing CSV files
- Add thresholds and warnings to categories
- Define budget goals
- Define recurrent expenses/income and add them automatically (weekly, monthly, annually)
- Support multiple currencies