package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.CategoryDto;
import wantsome.project.db.service.CategoriesDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class CategoriesPageController {

    public static String showCategoriesPage(Request req, Response res) {
        List<CategoryDto> allCategories = CategoriesDao.getAll();
        String errorMessage = req.queryParams("error");

        Map<String, Object> model = new HashMap<>();
        model.put("allCategories", allCategories);
        model.put("errorMessage", errorMessage);

        return render(model, "categories.vm");
    }

    public static Object handleDeleteRequest(Request req, Response res) {
        String id = req.params("id");
        String errorMessage = "";
        try {
            CategoriesDao.delete(Long.parseLong(id));
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        res.redirect("/categories?error=" + errorMessage);
        return res;
    }
}
