package wantsome.project.web;

import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionFullDto;
import wantsome.project.db.service.TransactionsDao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.business.TransactionsStatistics.*;
import static wantsome.project.web.SparkUtil.render;

public class ReportsPageController {

    private static MathTool mt = new MathTool();
    private static NumberTool nt = new NumberTool();

    public static String showReportsPage(Request req, Response res) {

        List<TransactionFullDto> transactionsNMonths;
        Date toDateSql = Date.valueOf(LocalDate.now());
        Date fromDateSql = Date.valueOf(LocalDate.now().minusMonths(5).withDayOfMonth(1));
        LocalDate adjustedToDate = toDateSql.toLocalDate();
        LocalDate adjustedFromDate = fromDateSql.toLocalDate();

        try {
            fromDateSql = Date.valueOf(req.queryParams("fromDate"));
            toDateSql = Date.valueOf(req.queryParams("toDate"));

            adjustedFromDate = fromDateSql.toLocalDate().withDayOfMonth(1);
            adjustedToDate = toDateSql.toLocalDate();
            adjustedToDate = adjustedToDate.withDayOfMonth(adjustedToDate.lengthOfMonth());

            fromDateSql = Date.valueOf(adjustedFromDate);
            toDateSql = Date.valueOf(adjustedToDate);

            transactionsNMonths = TransactionsDao.getPerDateInterval(fromDateSql, toDateSql);
        } catch (Exception e) {
            transactionsNMonths = TransactionsDao.getPerDateInterval(fromDateSql, toDateSql);
        }

        Map<String, Object> model = new HashMap<>();
        model.put("incomeLastNMonths", totalIncomePerMonthPerDateInterval(
                transactionsNMonths,
                adjustedFromDate, adjustedToDate
        ));
        model.put("expensesLastNMonths", totalExpensesPerMonthPerDateInterval(
                transactionsNMonths,
                adjustedFromDate, adjustedToDate
        ));
        model.put("balanceLastNMonths", balancePerMonthPerDateInterval(
                transactionsNMonths,
                adjustedFromDate, adjustedToDate
        ));
        model.put("balance", balancePerDateInterval(transactionsNMonths));
        model.put("income", totalIncomePerDateInterval(transactionsNMonths));
        model.put("expenses", totalExpensesPerDateInterval(transactionsNMonths));
        model.put("fromDate", fromDateSql);
        model.put("toDate", toDateSql);
        model.put("mt", mt);
        model.put("nt", nt);

        return render(model, "reports.vm");
    }

}
