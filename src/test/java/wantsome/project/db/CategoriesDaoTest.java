package wantsome.project.db;

import org.junit.*;
import org.junit.rules.ExpectedException;
import wantsome.project.db.dto.CategoryDto;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.CategoriesDao;
import wantsome.project.db.service.DbInitService;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CategoriesDaoTest {

    private static final String TEST_DB_FILE = "test.db";

    private static final List<CategoryDto> categoriesSample = Arrays.asList(
            new CategoryDto("clothes", Type.EXPENSE),
            new CategoryDto("food", Type.EXPENSE),
            new CategoryDto("paycheck", Type.INCOME),
            new CategoryDto("other", Type.INCOME));

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE);
        DbInitService.createMissingTables();
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    @Before
    public void insertRowsBeforeTest() {
        assertTrue(CategoriesDao.getAll().isEmpty());
        for (CategoryDto cat : categoriesSample) CategoriesDao.insert(cat);
        List<CategoryDto> categoriesFromDb = CategoriesDao.getAll();
    }

    @After
    public void deleteRowsAfterTest() {
        for (CategoryDto cat : CategoriesDao.getAll()) CategoriesDao.delete(cat.getId());
        assertTrue(CategoriesDao.getAll().isEmpty());
    }

    @Test
    public void getAll() {
        List<CategoryDto> catFromDb = CategoriesDao.getAll();
        checkOnlyTheSampledCatArePresentInDb();
    }

    @Test
    public void get() {
        CategoryDto firstCat = CategoriesDao.getAll().get(0);
        assertEqualCategoriesExceptId(categoriesSample.get(0), CategoriesDao.get(firstCat.getId()).get());
    }

    @Test
    public void insert() {
        CategoryDto newCat = new CategoryDto("gym", Type.EXPENSE);
        CategoriesDao.insert(newCat);
        assertEquals(5, CategoriesDao.getAll().size());
        assertEqualCategoriesExceptId(newCat, CategoriesDao.getAll().get(2));
    }

    @Test
    public void insert_descriptionMustBeUnique() {
        CategoryDto newCat = new CategoryDto("clothes", Type.INCOME);
        List<CategoryDto> catBeforeInsert = CategoriesDao.getAll();
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("category with same description already exists");
        CategoriesDao.insert(newCat);
    }

    @Test
    public void updateDescription() {
        CategoryDto cat1fromDb = CategoriesDao.getAll().get(0);
        CategoryDto updatedCategory = new CategoryDto(cat1fromDb.getId(), "apparel", cat1fromDb.getType());
        CategoriesDao.updateDescription("apparel", cat1fromDb.getId());
        assertEquals(updatedCategory, CategoriesDao.getAll().get(0));
    }

    @Test
    public void updateDescription_mustBeUnique() {
        CategoryDto cat1fromDb = CategoriesDao.getAll().get(0);
        String existingDescription = CategoriesDao.getAll().get(1).getDescription();
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("category with same description already exists");
        CategoriesDao.updateDescription(existingDescription, cat1fromDb.getId());
    }

    @Test
    public void updateDescription_forInvalidId() {
        CategoriesDao.updateDescription("pets", -90);
        checkOnlyTheSampledCatArePresentInDb();
    }

    @Test
    public void delete() {
        List<CategoryDto> catBeforeDelete = CategoriesDao.getAll();
        CategoriesDao.delete(catBeforeDelete.get(0).getId());
        List<CategoryDto> catAfterDelete = CategoriesDao.getAll();
        assertEquals(3, catAfterDelete.size());
        assertEqualCategoriesExceptId(catBeforeDelete.get(1), catAfterDelete.get(0));
        assertEqualCategoriesExceptId(catBeforeDelete.get(2), catAfterDelete.get(1));
        assertEqualCategoriesExceptId(catBeforeDelete.get(3), catAfterDelete.get(2));
    }

    @Test
    public void delete_forInvalidId() {
        CategoriesDao.delete(-90);
        checkOnlyTheSampledCatArePresentInDb();
    }

    public void assertEqualCategoriesExceptId(CategoryDto c1, CategoryDto c2) {
        assertTrue("Categories should be equal (except id): " + c1 + ", " + c2,
                c1.getDescription().equals(c2.getDescription()) &&
                        c1.getType().equals(c2.getType()));
    }

    private void checkOnlyTheSampledCatArePresentInDb() {
        List<CategoryDto> catFromDb = CategoriesDao.getAll();
        assertEquals(4, catFromDb.size());
        assertEqualCategoriesExceptId(categoriesSample.get(0), catFromDb.get(0));
        assertEqualCategoriesExceptId(categoriesSample.get(1), catFromDb.get(1));
        assertEqualCategoriesExceptId(categoriesSample.get(2), catFromDb.get(2));
        assertEqualCategoriesExceptId(categoriesSample.get(3), catFromDb.get(3));
    }
}
