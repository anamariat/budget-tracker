package wantsome.project.web;

import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionFullDto;
import wantsome.project.db.service.TransactionsDao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.business.TransactionsStatistics.*;
import static wantsome.project.web.SparkUtil.render;

public class MainPageController {

    private static NumberTool nt = new NumberTool();
    private static MathTool mt = new MathTool();

    public static String showMainPage(Request req, Response res) {

        LocalDate todayDate = LocalDate.now();
        LocalDate firstDayOfMonth = todayDate.withDayOfMonth(1);

        Date todayDateSql = Date.valueOf(todayDate);
        Date firstDayOfMonthSql = Date.valueOf(firstDayOfMonth);

        List<TransactionFullDto> transactionsCurrentMonth =
                TransactionsDao.getPerDateInterval(firstDayOfMonthSql, todayDateSql);

        List<TransactionFullDto> allTransactions = TransactionsDao.getAll();
        List<TransactionFullDto> last6Transactions = new ArrayList<>();
        for (int i = allTransactions.size() - 1; i > allTransactions.size() - 7; i--) {
            last6Transactions.add(allTransactions.get(i));
        }

        Map<String, Object> model = new HashMap<>();
        model.put("expensesCurrentMonthReport", totalExpensesPerEachCategory(transactionsCurrentMonth));
        model.put("incomeCurrentMonthReport", totalIncomePerEachCategory(transactionsCurrentMonth));
        model.put("incomeCurrentMonth", totalIncomePerDateInterval(transactionsCurrentMonth));
        model.put("expensesCurrentMonth", totalExpensesPerDateInterval(transactionsCurrentMonth));
        model.put("last6Transactions", last6Transactions);
        model.put("currentMonth", todayDate.getMonth());
        model.put("currentYear", todayDate.getYear());
        model.put("nt", nt);
        model.put("mt", mt);
        return render(model, "main.vm");
    }
}
