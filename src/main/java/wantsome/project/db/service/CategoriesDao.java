package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.CategoryDto;
import wantsome.project.db.dto.Type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static wantsome.project.db.service.tables.CategoriesTable.*;

public class CategoriesDao {

    private static CategoryDto extractCategoryFromResult(ResultSet r) throws SQLException {

        long id = r.getLong(FLD_CAT_ID);
        String description = r.getString(FLD_CAT_DESCRIPTION);
        Type type = Type.valueOf(r.getString(FLD_CAT_TYPE));

        return new CategoryDto(id, description, type);
    }

    public static List<CategoryDto> getAll() {

        String sql = "SELECT * FROM " + CATEGORIES_TABLE_NAME +
                " ORDER BY " + FLD_CAT_TYPE;

        List<CategoryDto> allCategories = new ArrayList<>();

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                allCategories.add(extractCategoryFromResult(rs));
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while loading categories: " + e.getLocalizedMessage());
        }

        return allCategories;
    }

    public static Optional<CategoryDto> get(long id) {

        String sql = "SELECT C.* " +
                "FROM " + CATEGORIES_TABLE_NAME + " C " +
                " WHERE C." + FLD_CAT_ID + " = ?";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    CategoryDto category = extractCategoryFromResult(rs);
                    return Optional.of(category);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error while loading category " + id + ": " + e.getLocalizedMessage());
        }

        return Optional.empty();
    }

    public static void insert(CategoryDto category) {

        String sql = "INSERT INTO " + CATEGORIES_TABLE_NAME + " (" +
                FLD_CAT_DESCRIPTION + ", " + FLD_CAT_TYPE + ")" +
                " VALUES (?,?)";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setString(1, category.getDescription());
            ps.setString(2, category.getType().name());

            ps.execute();

        } catch (SQLException e) {
            String message = e.getMessage().contains("UNIQUE constraint failed: CATEGORIES.DESCRIPTION") ?
                    "category with same description already exists" :
                    e.getLocalizedMessage();
            throw new RuntimeException("Error while saving new category: " + message);
        }
    }

    public static void updateDescription(String description, long id) {

        String sql = "UPDATE " + CATEGORIES_TABLE_NAME +
                " SET " + FLD_CAT_DESCRIPTION + " = ?" +
                " WHERE " + FLD_CAT_ID + " = ?";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setString(1, description);
            ps.setLong(2, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            String message = e.getMessage().contains("UNIQUE constraint failed: CATEGORIES.DESCRIPTION") ?
                    "category with same description already exists" :
                    e.getLocalizedMessage();
            throw new RuntimeException("Error while updating category " + id + ": " + message);
        }
    }

    public static void delete(long id) {

        String sql = "DELETE FROM " + CATEGORIES_TABLE_NAME +
                " WHERE " + FLD_CAT_ID + " = ?";

        try (Connection c = DbManager.getConnection();
             PreparedStatement ps = c.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            String message = e.getMessage().contains("FOREIGN KEY constraint failed") ?
                    "category is used by existing transactions" :
                    e.getLocalizedMessage();
            throw new RuntimeException("Error while deleting category " + id + ": " + message);
        }
    }
}

