package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class TransactionDto {
    private final long id;
    private final long categoryId;
    private final Date date;
    private final String details;
    private final double amount;

    public TransactionDto(long id, long categoryId, Date date, String details, double amount) {
        this.id = id;
        this.categoryId = categoryId;
        this.date = date;
        this.details = details;
        this.amount = amount;
    }

    public TransactionDto(long categoryId, Date date, String details, double amount) {
        this(-1, categoryId, date, details, amount);
    }

    public long getId() {
        return id;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public Date getDate() {
        return date;
    }

    public String getDetails() {
        if (details == null) {
            return "";
        }
        return details;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionDto that = (TransactionDto) o;
        return id == that.id &&
                categoryId == that.categoryId &&
                Double.compare(that.amount, amount) == 0 &&
                Objects.equals(date, that.date) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryId, date, details, amount);
    }

    @Override
    public String toString() {
        return "TransactionDto{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", date=" + date +
                ", details='" + details + '\'' +
                ", amount=" + amount +
                '}';
    }
}
