package wantsome.project.db;

import org.junit.*;
import wantsome.project.db.dto.TransactionDto;
import wantsome.project.db.dto.TransactionFullDto;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.DbInitService;
import wantsome.project.db.service.TransactionsDao;

import java.io.File;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class TransactionsDaoTest {

    private static final String TEST_DB_FILE = "test.db";

    private static final long DEFAULT_CAT_EXPENSE_ID = 1;
    private static final long DEFAULT_CAT_INCOME_ID = 9;
    private static final long baseTime = System.currentTimeMillis();
    List<TransactionDto> transactionsSample = Arrays.asList(
            new TransactionDto(DEFAULT_CAT_EXPENSE_ID, new Date(baseTime), "detail1", 89),
            new TransactionDto(DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 86400000), "detail2", 35),
            new TransactionDto(DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 86400000 * 2), null, 1500),
            new TransactionDto(DEFAULT_CAT_INCOME_ID, new Date(baseTime + 86400000), null, 9000));

    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE);
        DbInitService.createTablesAndInitialData();
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    @Before
    public void insertRowsBeforeTest() {
        for (TransactionFullDto trans : TransactionsDao.getAll()) TransactionsDao.delete(trans.getId());
        assertTrue(TransactionsDao.getAll().isEmpty());
        for (TransactionDto trans : transactionsSample) TransactionsDao.insert(trans);
    }

    @After
    public void deleteRowsAfterTest() {
        for (TransactionFullDto trans : TransactionsDao.getAll()) TransactionsDao.delete(trans.getId());
        assertTrue(TransactionsDao.getAll().isEmpty());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampledTransArePresentInDb();
    }

    @Test
    public void get() {
        TransactionFullDto trans1fromDb = TransactionsDao.getAll().get(0);
        assertEqualTransactionsExceptId(transactionsSample.get(2), TransactionsDao.get(trans1fromDb.getId()).get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(TransactionsDao.get(-90).isPresent());
    }

    @Test
    public void insert() {
        TransactionDto newTrans = new TransactionDto(
                DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 86400000 * 3), "inserted", 80);
        TransactionsDao.insert(newTrans);

        assertEquals(5, TransactionsDao.getAll().size());
        assertEqualTransactionsExceptId(newTrans, TransactionsDao.getAll().get(0));
    }

    @Test
    public void insert_nullDetails() {
        TransactionDto newTrans = new TransactionDto(
                DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 86400000 * 3), null, 80);
        TransactionsDao.insert(newTrans);

        assertEquals(5, TransactionsDao.getAll().size());
        assertEqualTransactionsExceptId(newTrans, TransactionsDao.getAll().get(0));
    }

    @Test
    public void update() {
        TransactionFullDto trans1fromDb = TransactionsDao.getAll().get(0);
        TransactionDto updatedTransaction = new TransactionDto(
                trans1fromDb.getId(), DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 86400000 * 3), "updated", 18);
        TransactionsDao.update(updatedTransaction);
        assertEqualTransactionsExceptId(updatedTransaction, TransactionsDao.getAll().get(0));
    }

    @Test
    public void update_forInvalidId() {
        TransactionDto updatedTransaction = new TransactionDto(
                -77, DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 500000), "updated", 18);
        TransactionsDao.update(updatedTransaction);
        checkOnlyTheSampledTransArePresentInDb();
    }

    @Test
    public void delete() {
        List<TransactionFullDto> transFromDb = TransactionsDao.getAll();
        TransactionsDao.delete(transFromDb.get(0).getId());
        List<TransactionFullDto> transInDbAfterDelete = TransactionsDao.getAll();
        assertEqualTransactionsExceptId(transFromDb.get(1), transInDbAfterDelete.get(0));
        assertEqualTransactionsExceptId(transFromDb.get(2), transInDbAfterDelete.get(1));
        assertEqualTransactionsExceptId(transFromDb.get(3), transInDbAfterDelete.get(2));
    }

    @Test
    public void delete_forInvalidId() {
        TransactionsDao.delete(-99);
        checkOnlyTheSampledTransArePresentInDb();
    }

    @Test
    public void getByType_expense() {
        List<TransactionFullDto> expenseTrans = TransactionsDao.getByType(Type.EXPENSE, null, null);
        assertEquals(3, expenseTrans.size());
        assertEqualTransactionsExceptId(transactionsSample.get(0), expenseTrans.get(2));
        assertEqualTransactionsExceptId(transactionsSample.get(1), expenseTrans.get(1));
        assertEqualTransactionsExceptId(transactionsSample.get(2), expenseTrans.get(0));
    }

    @Test
    public void getByType_income() {
        List<TransactionFullDto> incomeTrans = TransactionsDao.getByType(Type.INCOME, null, null);
        assertEquals(1, incomeTrans.size());
        assertEqualTransactionsExceptId(transactionsSample.get(3), incomeTrans.get(0));
    }

    @Test
    public void getByCategory() {
        List<TransactionFullDto> transByDescription = TransactionsDao.getByCategory("Housing & Utilities", null, null);
        assertEquals(3, transByDescription.size());
        assertEqualTransactionsExceptId(transactionsSample.get(2), transByDescription.get(0));
        assertEqualTransactionsExceptId(transactionsSample.get(1), transByDescription.get(1));
        assertEqualTransactionsExceptId(transactionsSample.get(0), transByDescription.get(2));
    }

    @Test
    public void getPerDateInterval() {
        List<TransactionFullDto> transPerDateInt = TransactionsDao.getPerDateInterval(new Date(baseTime - 86400000), new Date(baseTime));
        assertEquals(2, transPerDateInt.size());
        assertEqualTransactionsExceptId(transactionsSample.get(0), transPerDateInt.get(1));
        assertEqualTransactionsExceptId(transactionsSample.get(1), transPerDateInt.get(0));
    }

    @Test
    public void getPerDateInterval_intervalWithoutTrans() {
        List<TransactionFullDto> transPerDateInt = TransactionsDao.getPerDateInterval(
                new Date(baseTime - 86400000 * 4), new Date(baseTime - 86400000 * 3));
        assertEquals(0, transPerDateInt.size());
    }

    private void checkOnlyTheSampledTransArePresentInDb() {
        List<TransactionFullDto> transFromDb = TransactionsDao.getAll();
        assertEquals(4, transFromDb.size());
        assertEqualTransactionsExceptId(transactionsSample.get(2), transFromDb.get(0));
        assertEqualTransactionsExceptId(transactionsSample.get(1), transFromDb.get(1));
        assertEqualTransactionsExceptId(transactionsSample.get(0), transFromDb.get(2));
        assertEqualTransactionsExceptId(transactionsSample.get(3), transFromDb.get(3));
    }

    private void assertEqualTransactionsExceptId(TransactionFullDto t1, TransactionFullDto t2) {
        assertTrue("Transactions should be equal (except id): " + t1 + ", " + t2,
                t1.getCategoryId() == t2.getCategoryId() &&
                        t1.getCategoryDescription().equals(t2.getCategoryDescription()) &&
                        t1.getType().equals(t2.getType()) &&
                        t1.getDate().equals(t2.getDate()) &&
                        t1.getDetails().equals(t2.getDetails()) &&
                        t1.getAmount() == t2.getAmount());
    }

    private void assertEqualTransactionsExceptId(TransactionDto t1, TransactionDto t2) {
        assertTrue("Transactions should be equal (except id): " + t1 + ", " + t2,
                t1.getCategoryId() == t2.getCategoryId() &&
                        t1.getDate().equals(t2.getDate()) &&
                        t1.getDetails().equals(t2.getDetails()) &&
                        t1.getAmount() == t2.getAmount());
    }

    private void assertEqualTransactionsExceptId(TransactionDto t1, TransactionFullDto t2) {
        assertTrue("Transactions should be equal (except id): " + t1 + ", " + t2,
                t1.getCategoryId() == t2.getCategoryId() &&
                        t1.getDate().equals(t2.getDate()) &&
                        t1.getDetails().equals(t2.getDetails()) &&
                        t1.getAmount() == t2.getAmount());
    }
}