package wantsome.project.business;

import org.junit.*;
import wantsome.project.db.DbManager;
import wantsome.project.db.dto.TransactionDto;
import wantsome.project.db.dto.TransactionFullDto;
import wantsome.project.db.service.DbInitService;
import wantsome.project.db.service.TransactionsDao;

import java.io.File;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TransactionsStatisticsTest {

    private static final String TEST_DB_FILE = "test.db";

    private static final long DEFAULT_CAT_EXPENSE_ID = 1;
    private static final long DEFAULT_CAT_INCOME_ID = 9;
    private static final long baseTime = System.currentTimeMillis();
    List<TransactionDto> transactionsSample = Arrays.asList(
            new TransactionDto(DEFAULT_CAT_EXPENSE_ID, new Date(baseTime), "detail1", 89),
            new TransactionDto(DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 86400000), "detail2", 35),
            new TransactionDto(DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 86400000 * 2), null, 1500),
            new TransactionDto(DEFAULT_CAT_EXPENSE_ID, new Date(baseTime - 2592000444L), null, 2300),
            new TransactionDto(DEFAULT_CAT_INCOME_ID, new Date(baseTime + 86400000), null, 1500));

    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE);
        DbInitService.createTablesAndInitialData();
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    @Before
    public void insertRowsBeforeTest() {
        if (!TransactionsDao.getAll().isEmpty()) {
            for (TransactionFullDto trans : TransactionsDao.getAll())
                TransactionsDao.delete(trans.getId());
        }
        assertTrue(TransactionsDao.getAll().isEmpty());
        for (TransactionDto trans : transactionsSample) TransactionsDao.insert(trans);
    }

    @After
    public void deleteRowsAfterTest() {
        for (TransactionFullDto trans : TransactionsDao.getAll()) TransactionsDao.delete(trans.getId());
        assertTrue(TransactionsDao.getAll().isEmpty());
    }

    @Test
    public void totalIncomePerDateInterval() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime), new Date(baseTime + 86400000));
        double totalIncome = TransactionsStatistics.totalIncomePerDateInterval(transactions);
        assertEquals(1500, totalIncome, 0.001);
    }

    @Test
    public void totalIncomePerDateInterval_intervalWithoutTrans() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime + 86400000 * 3), new Date(baseTime + 86400000 * 4));
        double totalIncome = TransactionsStatistics.totalIncomePerDateInterval(transactions);
        assertEquals(0, totalIncome, 0.001);
    }

    @Test
    public void totalIncomePerDateInterval_intervalWithExpensesOnly() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime - 86400000 * 3), new Date(baseTime));
        double totalIncome = TransactionsStatistics.totalIncomePerDateInterval(transactions);
        assertEquals(0, totalIncome, 0.001);
    }

    @Test
    public void totalExpensesPerDateInterval() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime - 86400000), new Date(baseTime + 86400000));
        double totalExpenses = TransactionsStatistics.totalExpensesPerDateInterval(transactions);
        assertEquals(89 + 35, totalExpenses, 0.001);
    }

    @Test
    public void totalExpensesPerDateInterval_intervalWithoutTrans() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime + 86400000 * 3), new Date(baseTime + 86400000 * 4));
        double totalExpenses = TransactionsStatistics.totalExpensesPerDateInterval(transactions);
        assertEquals(0, totalExpenses, 0.001);
    }

    @Test
    public void totalExpensesPerDateInterval_intervalWithIncomeOnly() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime + 86400000), new Date(baseTime + 86400000 * 2));
        double totalExpenses = TransactionsStatistics.totalExpensesPerDateInterval(transactions);
        assertEquals(0, totalExpenses, 0.001);
    }

    @Test
    public void balancePerDateInterval() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime - 86400000), new Date(baseTime + 86400000));
        double balance = TransactionsStatistics.balancePerDateInterval(transactions);
        assertEquals(1500 - 89 - 35, balance, 0.001);
    }

    @Test
    public void balancePerDateInterval_intervalWithoutTrans() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime + 86400000 * 3), new Date(baseTime + 86400000 * 4));
        double balance = TransactionsStatistics.balancePerDateInterval(transactions);
        assertEquals(0, balance, 0.001);
    }

    @Test
    public void balancePerMonthPerDateInterval() {
        Map<String, Double> result =
                TransactionsStatistics.balancePerMonthPerDateInterval(
                        TransactionsDao.getAll(),
                        new Date(baseTime - 2592000444L).toLocalDate(),
                        new Date(baseTime + 86400000).toLocalDate());
        List<Double> values = Arrays.asList(-2300D, 1500 - 35 - 89 - 1500D);

        assertEquals(2, result.size());

        int i = 0;
        for (Map.Entry<String, Double> e : result.entrySet()) {
            assertEquals(values.get(i), e.getValue());
            i++;
        }
    }

    @Test
    public void totalIncomePerMonthPerDateInterval() {
        Map<String, Double> result =
                TransactionsStatistics.totalIncomePerMonthPerDateInterval(
                        TransactionsDao.getAll(),
                        new Date(baseTime - 2592000444L).toLocalDate(),
                        new Date(baseTime + 86400000).toLocalDate());
        List<Double> values = Arrays.asList(0D, 1500D);

        assertEquals(2, result.size());

        int i = 0;
        for (Map.Entry<String, Double> e : result.entrySet()) {
            assertEquals(values.get(i), e.getValue());
            i++;
        }
    }

    @Test
    public void totalExpensesPerMonthPerDateInterval() {
        Map<String, Double> result =
                TransactionsStatistics.totalExpensesPerMonthPerDateInterval(
                        TransactionsDao.getAll(),
                        new Date(baseTime - 2592000444L).toLocalDate(),
                        new Date(baseTime + 86400000).toLocalDate());
        List<Double> values = Arrays.asList(2300D, 89 + 35 + 1500D);

        assertEquals(2, result.size());

        int i = 0;
        for (Map.Entry<String, Double> e : result.entrySet()) {
            assertEquals(values.get(i), e.getValue());
            i++;
        }
    }

    @Test
    public void totalExpensesPerEachCategory() {
        Map<String, Double> result =
                TransactionsStatistics.totalExpensesPerEachCategory(
                        TransactionsDao.getAll());
        Double value = 2300 + 89 + 35 + 1500D;

        assertEquals(1, result.size());

        for (Map.Entry<String, Double> e : result.entrySet()) {
            assertEquals(value, e.getValue());
        }
    }

    @Test
    public void totalIncomePerEachCategory() {
        Map<String, Double> result =
                TransactionsStatistics.totalIncomePerEachCategory(
                        TransactionsDao.getAll());
        Double value = 1500D;

        assertEquals(1, result.size());

        for (Map.Entry<String, Double> e : result.entrySet()) {
            assertEquals(value, e.getValue());
        }
    }

    @Test
    public void totalPerEachCategory_intervalWithoutTrans() {
        List<TransactionFullDto> transactions = TransactionsDao.getPerDateInterval(
                new Date(baseTime + 86400000 * 2), new Date(baseTime + 86400000 * 3));
        Map<String, Double> result = TransactionsStatistics.totalExpensesPerEachCategory(transactions);
        assertEquals(0, result.size());
    }


}
