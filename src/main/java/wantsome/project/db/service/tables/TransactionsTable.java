package wantsome.project.db.service.tables;

public class TransactionsTable {

    public static final String TRANSACTIONS_TABLE_NAME = "TRANSACTIONS";

    public static final String FLD_TRANS_ID = "ID";
    public static final String FLD_TRANS_CAT_ID = "CATEGORY_ID";
    public static final String FLD_TRANS_DATE = "DATE";
    public static final String FLD_TRANS_DETAILS = "DETAILS";
    public static final String FLD_TRANS_AMOUNT = "AMOUNT";
}

