package wantsome.project.db.dto;

public enum Type {
    INCOME("Income"),
    EXPENSE("Expense");

    private final String label;

    Type(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
