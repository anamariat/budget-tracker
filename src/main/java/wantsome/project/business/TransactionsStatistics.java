package wantsome.project.business;

import wantsome.project.db.dto.TransactionFullDto;
import wantsome.project.db.dto.Type;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;

public class TransactionsStatistics {

    public static double totalIncomePerDateInterval(List<TransactionFullDto> transactions) {

        double totalIncome = 0;

        for (TransactionFullDto transaction : transactions) {
            if (transaction.getType().equals(Type.INCOME)) {
                totalIncome += transaction.getAmount();
            }
        }

        return totalIncome;
    }

    public static double totalExpensesPerDateInterval(List<TransactionFullDto> transactions) {

        double totalExpenses = 0;

        for (TransactionFullDto transaction : transactions) {
            if (transaction.getType().equals(Type.EXPENSE)) {
                totalExpenses += transaction.getAmount();
            }
        }

        return totalExpenses;
    }

    public static double balancePerDateInterval(List<TransactionFullDto> transactions) {

        double total = 0;

        for (TransactionFullDto transaction : transactions) {
            if (transaction.getType().equals(Type.EXPENSE)) {
                total -= transaction.getAmount();
            } else {
                total += transaction.getAmount();
            }
        }

        return total;
    }

    public static Map<String, Double> totalExpensesPerEachCategory(List<TransactionFullDto> transactions) {

        return transactions.stream()
                .filter(t -> t.getType().toString().equalsIgnoreCase("expense"))
                .collect(groupingBy(
                        TransactionFullDto::getCategoryDescription,
                        TreeMap::new,
                        summingDouble(TransactionFullDto::getAmount)));
    }

    public static Map<String, Double> totalIncomePerEachCategory(List<TransactionFullDto> transactions) {

        return transactions.stream()
                .filter(t -> t.getType().toString().equalsIgnoreCase("income"))
                .collect(groupingBy(
                        TransactionFullDto::getCategoryDescription,
                        TreeMap::new,
                        summingDouble(TransactionFullDto::getAmount)));
    }

    public static Map<String, Double> balancePerMonthPerDateInterval(
            List<TransactionFullDto> transactions,
            LocalDate fromDate, LocalDate toDate) {

        Map<String, Double> balancePerMonth = new LinkedHashMap<>();

        LocalDate currentDate = fromDate;
        while (currentDate.isBefore(toDate) || currentDate.isEqual(toDate)) {
            balancePerMonth.put(currentDate.getMonth().toString().substring(0, 3), 0.0);
            currentDate = currentDate.plusMonths(1);
        }

        for (TransactionFullDto transaction : transactions) {
            String month =
                    transaction.getDate().toLocalDate().getMonth().toString().substring(0, 3);
            if (transaction.getType().equals(Type.INCOME)) {
                balancePerMonth.put(month, balancePerMonth.get(month) + transaction.getAmount());
            } else {
                balancePerMonth.put(month, balancePerMonth.get(month) - transaction.getAmount());
            }
        }
        return balancePerMonth;
    }

    public static Map<String, Double> totalIncomePerMonthPerDateInterval(
            List<TransactionFullDto> transactions,
            LocalDate fromDate, LocalDate toDate) {
        return totalTypePerMonthPerDateInterval(transactions, Type.INCOME, fromDate, toDate);
    }

    public static Map<String, Double> totalExpensesPerMonthPerDateInterval(
            List<TransactionFullDto> transactions,
            LocalDate fromDate, LocalDate toDate) {
        return totalTypePerMonthPerDateInterval(transactions, Type.EXPENSE, fromDate, toDate);
    }

    private static Map<String, Double> totalTypePerMonthPerDateInterval(
            List<TransactionFullDto> transactions, Type type,
            LocalDate fromDate, LocalDate toDate) {

        Map<String, Double> totalTypePerMonth = new LinkedHashMap<>();

        LocalDate currentDate = fromDate;
        while (currentDate.isBefore(toDate) || currentDate.isEqual(toDate)) {
            totalTypePerMonth.put(currentDate.getMonth().toString().substring(0, 3), 0.0);
            currentDate = currentDate.plusMonths(1);
        }

        for (TransactionFullDto transaction : transactions) {
            if (transaction.getType().equals(type)) {
                String month =
                        transaction.getDate().toLocalDate().getMonth().toString().substring(0, 3);

                totalTypePerMonth.put(month, totalTypePerMonth.get(month) + transaction.getAmount());
            }
        }
        return totalTypePerMonth;
    }
}
