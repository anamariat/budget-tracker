package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class TransactionFullDto {
    private final long id;
    private final long categoryId;
    private final String categoryDescription;
    private final Type type;
    private final Date date;
    private final String details;
    private final double amount;

    public TransactionFullDto(long id, long categoryId, String categoryDescription, Type type, Date date, String details, double amount) {
        this.id = id;
        this.categoryId = categoryId;
        this.categoryDescription = categoryDescription;
        this.type = type;
        this.date = date;
        this.details = details;
        this.amount = amount;
    }

    public TransactionFullDto(long categoryId, String categoryDescription, Type type, Date date, String details, double amount) {
        this(-1, categoryId, categoryDescription, type, date, details, amount);
    }

    public long getId() {
        return id;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public Type getType() {
        return type;
    }

    public Date getDate() {
        return date;
    }

    public String getDetails() {
        if (details == null) {
            return "";
        }
        return details;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionFullDto that = (TransactionFullDto) o;
        return id == that.id &&
                categoryId == that.categoryId &&
                Double.compare(that.amount, amount) == 0 &&
                Objects.equals(categoryDescription, that.categoryDescription) &&
                type == that.type &&
                Objects.equals(date, that.date) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryId, categoryDescription, type, date, details, amount);
    }

    @Override
    public String toString() {
        return "TransactionFullDto{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", categoryDescription=" + categoryDescription +
                ", type=" + type +
                ", date=" + date +
                ", details=" + details +
                ", amount=" + amount +
                '}';
    }
}
