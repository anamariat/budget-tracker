package wantsome.project;

import wantsome.project.db.service.DbInitService;
import wantsome.project.web.*;

import static spark.Spark.*;

public class Main {

    public static void main(String[] args) {

        DbInitService.createTablesAndInitialData();

        staticFileLocation("public");
        configureRoutesAndStart();

        awaitInitialization();
        System.out.println("Web app started, url: http://localhost:4567/main");
    }

    private static void configureRoutesAndStart() {

        get("/main", MainPageController::showMainPage);

        get("/transactions", TransactionsPageController::showTransactionsPage);

        get("/update/transaction/:id", AddEditTransactionPageController::showUpdateForm);
        post("/update/transaction/:id", AddEditTransactionPageController::handleAddUpdateRequest);

        get("/add/transaction", AddEditTransactionPageController::showAddForm);
        post("/add/transaction", AddEditTransactionPageController::handleAddUpdateRequest);

        get("/delete/transaction/:id", TransactionsPageController::handleDeleteRequest);

        get("/categories", CategoriesPageController::showCategoriesPage);

        get("/update/category/:id", AddEditCategoryPageController::showUpdateForm);
        post("/update/category/:id", AddEditCategoryPageController::handleAddUpdateRequest);

        get("/add/category", AddEditCategoryPageController::showAddForm);
        post("/add/category", AddEditCategoryPageController::handleAddUpdateRequest);

        get("/delete/category/:id", CategoriesPageController::handleDeleteRequest);

        get("/reports", ReportsPageController::showReportsPage);

        exception(Exception.class, ErrorPageController::handleException);
    }
}
