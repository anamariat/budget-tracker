package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.CategoryDto;
import wantsome.project.db.dto.TransactionDto;
import wantsome.project.db.dto.Type;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.project.db.service.tables.CategoriesTable.*;
import static wantsome.project.db.service.tables.TransactionsTable.*;

public class DbInitService {

    public static void createTablesAndInitialData() {
        createMissingTables();
        insertPredefinedCategories();
        addTransactionSampleIfDbEmpty();
    }

    public static void createMissingTables() {
        String sql1 = "CREATE TABLE IF NOT EXISTS " + CATEGORIES_TABLE_NAME + " (" +
                FLD_CAT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                FLD_CAT_DESCRIPTION + " TEXT UNIQUE NOT NULL, " +
                FLD_CAT_TYPE + " TEXT CHECK (" + FLD_CAT_TYPE + " IN ('" +
                Type.INCOME + "', '" + Type.EXPENSE + "')) NOT NULL);";

        String sql2 = "CREATE TABLE IF NOT EXISTS " + TRANSACTIONS_TABLE_NAME + " (" +
                FLD_TRANS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                FLD_TRANS_CAT_ID + " INT REFERENCES " + CATEGORIES_TABLE_NAME + "(ID) NOT NULL, " +
                FLD_TRANS_DATE + " DATETIME NOT NULL, " +
                FLD_TRANS_DETAILS + " TEXT, " +
                FLD_TRANS_AMOUNT + " REAL NOT NULL);";

        try (Connection c = DbManager.getConnection();
             Statement st = c.createStatement()) {
            st.execute(sql1);
            st.execute(sql2);
        } catch (SQLException e) {
            System.err.println("Error creating missing tables: " + e.getMessage());
        }
    }

    public static void insertPredefinedCategories() {
        if (CategoriesDao.getAll().isEmpty()) {
            CategoriesDao.insert(
                    new CategoryDto("Housing & Utilities",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto("Food",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto(
                            "Clothes",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto("Transportation",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto("Medical & Healthcare",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto("Personal care",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto("Memberships",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto("Other expenses",
                            Type.EXPENSE));
            CategoriesDao.insert(
                    new CategoryDto("Paycheck",
                            Type.INCOME));
            CategoriesDao.insert(
                    new CategoryDto("Other sources of income",
                            Type.INCOME));
        }
    }

    public static void addTransactionSampleIfDbEmpty() {
        if (TransactionsDao.getAll().isEmpty()) {
            TransactionsDao.insert(
                    new TransactionDto(9,
                            Date.valueOf("2020-03-12"),
                            null,
                            1000));
            TransactionsDao.insert(
                    new TransactionDto(1,
                            Date.valueOf("2020-03-14"),
                            "Electricity",
                            280));
            TransactionsDao.insert(
                    new TransactionDto(2,
                            Date.valueOf("2020-03-17"),
                            "Pizza",
                            39.9));
            TransactionsDao.insert(
                    new TransactionDto(2,
                            Date.valueOf("2020-03-20"),
                            "Hamburger",
                            30));
            TransactionsDao.insert(
                    new TransactionDto(3,
                            Date.valueOf("2020-03-22"),
                            "Jeans",
                            109));
            TransactionsDao.insert(
                    new TransactionDto(7,
                            Date.valueOf("2020-03-25"),
                            "Gym",
                            220));
            TransactionsDao.insert(
                    new TransactionDto(9,
                            Date.valueOf("2020-04-12"),
                            null,
                            1000));
            TransactionsDao.insert(
                    new TransactionDto(1,
                            Date.valueOf("2020-04-14"),
                            "Water",
                            38));
            TransactionsDao.insert(
                    new TransactionDto(7,
                            Date.valueOf("2020-04-29"),
                            "Scribd",
                            48.9));
            TransactionsDao.insert(
                    new TransactionDto(4,
                            Date.valueOf("2020-05-1"),
                            "Bus pass",
                            40));
            TransactionsDao.insert(
                    new TransactionDto(6,
                            Date.valueOf("2020-05-9"),
                            "Shower gel",
                            45.5));
            TransactionsDao.insert(
                    new TransactionDto(5,
                            Date.valueOf("2020-05-10"),
                            "Health Insurance",
                            200));
            TransactionsDao.insert(
                    new TransactionDto(8,
                            Date.valueOf("2020-05-3"),
                            "Gift",
                            150));
            TransactionsDao.insert(
                    new TransactionDto(10,
                            Date.valueOf("2020-05-5"),
                            "Lotto prize",
                            500));
            TransactionsDao.insert(
                    new TransactionDto(9,
                            Date.valueOf("2020-06-12"),
                            null,
                            1800));
            TransactionsDao.insert(
                    new TransactionDto(7,
                            Date.valueOf("2020-06-20"),
                            "Google Storage",
                            40));
            TransactionsDao.insert(
                    new TransactionDto(1,
                            Date.valueOf("2020-06-22"),
                            "Electricity",
                            300));
            TransactionsDao.insert(
                    new TransactionDto(9,
                            Date.valueOf("2020-07-5"),
                            null,
                            2000));
            TransactionsDao.insert(
                    new TransactionDto(2,
                            Date.valueOf("2020-07-4"),
                            "Grilled chicken breast",
                            40));
            TransactionsDao.insert(
                    new TransactionDto(6,
                            Date.valueOf("2020-07-6"),
                            "Sun cream",
                            51.2));
            TransactionsDao.insert(
                    new TransactionDto(10,
                            Date.valueOf("2020-07-7"),
                            "Gift",
                            200));
            TransactionsDao.insert(
                    new TransactionDto(7,
                            Date.valueOf("2020-07-5"),
                            null,
                            120));
            TransactionsDao.insert(
                    new TransactionDto(1,
                            Date.valueOf("2020-07-8"),
                            "Electricity",
                            275.99));

        }
    }
}

