package wantsome.project.db.service.tables;

public class CategoriesTable {

    public static final String CATEGORIES_TABLE_NAME = "CATEGORIES";

    public static final String FLD_CAT_ID = "ID";
    public static final String FLD_CAT_DESCRIPTION = "DESCRIPTION";
    public static final String FLD_CAT_TYPE = "TYPE";
}
